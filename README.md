# [partnerka.net](https://partnerka.net) source codes

<br/>

### Run partnerka.net on localhost

    # vi /etc/systemd/system/partnerka.net.service

Insert code from partnerka.net.service

    # systemctl enable partnerka.net.service
    # systemctl start partnerka.net.service
    # systemctl status partnerka.net.service

http://localhost:4038
